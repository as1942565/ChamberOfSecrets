﻿using UnityEngine;
using System.Collections;

public class CylinderCtl : MonoBehaviour 
{
	public GameObject CylinderP;
	
	public GameObject door;
	DoorCtl doorctl;
	
	void Start ()
	{
		doorctl = door.GetComponent<DoorCtl>();
	}
	
	void Update () 
	{
		
	}
	void OnMouseDown()
	{
		this.gameObject.SetActive( false );
		CylinderP.SetActive ( true );
		doorctl.Cylinder();
	}
}
