﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Key2Ctl : MonoBehaviour 
{
	private int Num = 0;
	private int TrueNum = 9;
	public Text text;
	
	public GameObject KeyUI;
	KeywordCtl keywordctl;
	
	void Start () 
	{
		keywordctl = KeyUI.GetComponent<KeywordCtl>();
	}
	
	void Update () 
	{
		if( Num == TrueNum )
		{
			keywordctl.TrueKey2 ( true );
		}
		else
		{
			keywordctl.TrueKey2 ( false );
		}
	}
	
	
	public void Up ()
	{
		Num += 1;
		if( Num >= 10 ) Num = 0;
		text.text = Num.ToString();
	}
	
	public void Down ()
	{
		Num -= 1;
		if( Num <= 0 ) Num = 9;
		text.text = Num.ToString();
	}
}
