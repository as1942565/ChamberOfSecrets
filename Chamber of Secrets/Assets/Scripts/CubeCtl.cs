﻿using UnityEngine;
using System.Collections;

public class CubeCtl : MonoBehaviour 
{
	public GameObject CubeP;

	public GameObject door;
	DoorCtl doorctl;

	void Start ()
	{
		doorctl = door.GetComponent<DoorCtl>();
	}

	void Update () 
	{
	
	}
	void OnMouseDown()
	{
		this.gameObject.SetActive( false );
		CubeP.SetActive ( true );
		doorctl.Cube();
	}

}
