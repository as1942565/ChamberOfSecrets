﻿using UnityEngine;
using System.Collections;

public class KeywordCtl : MonoBehaviour
{
	private static bool Tkey1;
	private static bool Tkey2;
	private static bool Tkey3;
	private static bool Tkey4;

	public GameObject KeyUI;
	public GameObject door;

	void Start () 
	{
	
	}

	void Update ()
	{
		if ( Tkey1 && Tkey2 && Tkey3 && Tkey4 )
		{
			KeyUI.SetActive ( false );
			door.transform.Rotate( 0 , -105 , 0 );
		}

	}

	public void TrueKey ( bool key1 )
	{
		Tkey1 = key1;
	}
	public void TrueKey2 ( bool key2 )
	{
		Tkey2 = key2;
	}
	public void TrueKey3 ( bool key3 )
	{
		Tkey3 = key3;
	}
	public void TrueKey4 ( bool key4 )
	{
		Tkey4 = key4;
	}
}
