﻿using UnityEngine;
using System.Collections;

public class SphereCtl : MonoBehaviour 
{
	public GameObject SphereP;

	public GameObject door;
	DoorCtl doorctl;
	
	void Start ()
	{
		doorctl = door.GetComponent<DoorCtl>();
	}
	void Update ()
	{
	
	}

	void OnMouseDown ()
	{
		this.gameObject.SetActive( false );
		SphereP.SetActive ( true );
		doorctl.Sphere();
	}
}
