﻿using UnityEngine;
using System.Collections;

public class GameUICtl : MonoBehaviour
{
	public GameObject MainCam;
	public GameObject Cam2;
	public GameObject DownArrow;

	void Start () 
	{
	
	}

	void Update ()
	{
	
	}

	public void Left ()
	{
		MainCam.transform.Rotate ( 0 , -90 , 0 , Space.World);
	}

	public void Right ()
	{
		MainCam.transform.Rotate ( 0 , 90 , 0 , Space.World);
	}
	public void Down()
	{
		Cam2.SetActive ( false );
		DownArrow.SetActive ( false );
	}
}
