﻿using UnityEngine;
using System.Collections;

public class DoorCtl : MonoBehaviour 
{
	private static bool CubeB = false;
	private static bool SphereB = false;
	private static bool CylinderB = false;

	private bool Win;
	public GameObject WinUI;
	public GameObject door;

	void Start ()
	{
	
	}

	void Update () 
	{
		if( CubeB && SphereB && CylinderB )
			Win = true;
	}

	void OnMouseDown ()
	{
		if ( Win )
		{
			WinUI.SetActive ( true );
			door.SetActive ( false );
		}
	}

	public void Cube ()
	{
		CubeB = true;
	}

	public void Sphere ()
	{
		SphereB = true;
	}

	public void Cylinder ()
	{
		CylinderB = true;
	}
}
